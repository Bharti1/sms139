package com.spice.sms139.db;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;

import com.spice.sms139.utilites.LogStackTrace;
import com.spice.sms139.utilites.Logs;
import com.spice.sms139.utilites.OsType;

public class DataBaseOperation {

	public String login(String strUserName, String strPassword) {
		String retVal = "";
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		ResultSet rs = null;
		try {
			String strQuery = "SELECT uniqueId FROM tbl_http_accounts where username=? and password=?";
			objConnection = DBManager.getConnection();
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, strUserName);
			objStatement.setString(2, strPassword);
			rs = objStatement.executeQuery();
			if (rs.next()) {
				retVal = rs.getString(1);
			}
		} catch (Exception ex) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(objConnection);
		}
		return retVal;
	}

	public String getAuthentication(String strUname, String strPasswd) {
		String strID = "";
		String strUnamePasswd = strUname + ":" + strPasswd;
		HashMap<String, String> hm = new HashMap<String, String>();
		FileInputStream fis = null;
		DataInputStream dis = null;
		BufferedReader br = null;
		try {
			String filePath = "/home/sms139_services/http_accounts.cfg";
			if (OsType.isWindows()) {
				filePath = "E:/sms139_services/http_accounts.cfg";
			}
			fis = new FileInputStream(filePath);
			dis = new DataInputStream(fis);
			br = new BufferedReader(new InputStreamReader(dis));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] parts = strLine.split("#");
				hm.put(parts[0], parts[1]);
			}
			strID = hm.get(strUnamePasswd);
		} catch (Exception e) {
			Logs.getErrorLogs(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(dis);
			IOUtils.closeQuietly(br);
		}
		if (strID == null) {
			strID = "OTHER";
		}
		return strID;
	}

	public String getStnCode(String strStnName) {
		String strStnCode = "";
		HashMap<String, String> hm = new HashMap<String, String>();
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try {

			String filePath = "/home/sms139_services/getStnCode.cfg";
			if (OsType.isWindows()) {
				filePath = "E:/sms139_services/getStnCode.cfg";
			}

			fstream = new FileInputStream(filePath);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] parts = strLine.split("#");
				hm.put(parts[0], parts[1]);
			}
			strStnCode = hm.get(strStnName);
		} catch (Exception e) {// Catch exception if any
			Logs.getErrorLogs(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fstream);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(br);
		}
		if (strStnCode == null) {
			strStnCode = "OTHER";
		}
		return strStnCode;
	}

	public String checkStnCode(String strStnName) {
		String strStnCode = "";
		HashMap<String, String> hm = new HashMap<String, String>();
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try {
			String filePath = "/home/sms139_services/checkStnCode.cfg";
			if (OsType.isWindows()) {
				filePath = "E:/sms139_services/checkStnCode.cfg";
			}

			fstream = new FileInputStream(filePath);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] parts = strLine.split("#");
				hm.put(parts[0], parts[1]);
			}
			strStnCode = hm.get(strStnName);
		} catch (Exception e) {// Catch exception if any
			Logs.getErrorLogs(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fstream);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(br);
		}
		if (strStnCode == null) {
			strStnCode = "OTHER";
		}
		return strStnCode;
	}

	public void insertRecord(String strMsisdn, String strSourceAdderss, String strMessage, String strUniqueId,
			String strDateTime, String strKeyword, String strUdh, String strDcs, String strVmn, String strImsi,
			String strCircleId, String strUserName, String strPassword) {
		strMessage = strMessage.replaceAll("'", "");
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strQuery = "insert into tbl_http_request (Msisdn, SourceAdderss, Message, UniqueId, DateTime, Keyword, Udh, "
					+ "Dcs, Vmn, Imsi, CircleId,userName, password) values(?,?,?,?,now(),?,?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, strMsisdn);
			objStatement.setString(2, strSourceAdderss);
			objStatement.setString(3, strMessage);
			objStatement.setString(4, strUniqueId);
			objStatement.setString(5, strKeyword);
			objStatement.setString(6, strUdh);
			objStatement.setString(7, strDcs);
			objStatement.setString(8, strVmn);
			objStatement.setString(9, strImsi);
			objStatement.setString(10, strCircleId);
			objStatement.setString(11, strUserName);
			objStatement.setString(12, strPassword);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(strUniqueId + "," + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(objConnection);
		}

	}

	public void insertTrainBetweenTwoStnInfo(String strId, String strMobile, String strMessage, String strResponse,
			int strTotalTrains) {
		System.out.println("Response message passed to DB === " + strResponse);
		strMessage = strMessage.replaceAll("'", "");
		strResponse = strResponse.replaceAll("'", "");
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strQuery = "INSERT INTO tbl_train_two_station(date_time,message_id,mobile,req_message,resp_message,total_trains,"
					+ "sms_sent,STATUS) VALUES (now(),?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strQuery);
			objStatement.setString(1, strId);
			objStatement.setString(2, strMobile);
			objStatement.setString(3, strMessage);
			objStatement.setString(4, strResponse);
			objStatement.setInt(5, strTotalTrains);
			objStatement.setString(6, "1");
			objStatement.setString(7, "A");
			objStatement.executeUpdate();
		} catch (Exception ex) {
			Logs.getErrorLogs(strId + "," + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(objConnection);
		}
	}

	public void insertRequestLog(String strDateTime, String strId, String strUniqueId, String strMsisdn,
			String strMessage) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();

			if (strMessage.startsWith("PAY") || strMessage.startsWith("PT ")) {
				strMessage = strMessage.replaceAll("\\s+", " ");
				String[] parts = strMessage.split("\\s");

				List<String> trimmedParts = new ArrayList<String>();

				for (String part : parts) {
					if (part.trim().length() > 0) {
						trimmedParts.add(part);
					}
				}
				if (trimmedParts.size() > 3) {
					String keyword = trimmedParts.get(0);
					String TID = trimmedParts.get(1);
					String mode = trimmedParts.get(2);
					strMessage = keyword + " " + TID + " " + mode + " XXXX XXXXX XXXXX";
				}
			} else if (strMessage.startsWith("CAN") || strMessage.startsWith("CT ")) {
				strMessage = strMessage.replaceAll("\\s+", " ");
				String[] parts = strMessage.split("\\s");

				List<String> trimmedParts = new ArrayList<String>();

				for (String part : parts) {
					if (part.trim().length() > 0) {
						trimmedParts.add(part);
					}
				}
				if (trimmedParts.size() > 2) {
					String keyword = trimmedParts.get(0);
					String PNR = trimmedParts.get(1);
					strMessage = keyword + " " + PNR + " XXXXXXXXX";
				}
			}
			String strInsertQuery = "insert into tbl_http_request_log(date_time,service_type,message_id,identifier,mobile,source,"
					+ "message,keyword) VALUES(NOW(),?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, "HTTP");
			objStatement.setString(2, strId);
			objStatement.setString(3, strUniqueId);
			objStatement.setString(4, strMsisdn);
			objStatement.setString(5, "139");
			objStatement.setString(6, strMessage);
			objStatement.setString(7, "NA");
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(strId + "," + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(objConnection);
		}

	}


	public void insertResponsetLog(String strId, String strUniqueId, String strMsisdn, String strOutPut,
			String keyword) {
		Connection objConnection = null;
		PreparedStatement objStatement = null;
		try {
			objConnection = DBManager.getConnection();
			String strInsertQuery = "insert into tbl_http_response_log(date_time,service_type,message_id,identifier,mobile,"
					+ "source,message,keyword) values(now(),?,?,?,?,?,?,?)";
			objStatement = objConnection.prepareStatement(strInsertQuery);
			objStatement.setString(1, "HTTP");
			objStatement.setString(2, strId);
			objStatement.setString(3, strUniqueId);
			objStatement.setString(4, strMsisdn);
			objStatement.setString(5, "139");
			objStatement.setString(6, strOutPut);
			objStatement.setString(7, keyword);
			objStatement.executeUpdate();

		} catch (Exception ex) {
			Logs.getErrorLogs(strId + "," + strUniqueId + "," + LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			DbUtils.closeQuietly(objStatement);
			DbUtils.closeQuietly(objConnection);
		}

	}

}
