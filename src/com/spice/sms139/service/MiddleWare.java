package com.spice.sms139.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.spice.sms139.commons.CallBooking;
import com.spice.sms139.commons.FormatInputString;
import com.spice.sms139.commons.GetFooter;
import com.spice.sms139.commons.Response;
import com.spice.sms139.db.DataBaseOperation;
import com.spice.sms139.utilites.LogStackTrace;
import com.spice.sms139.utilites.Logs;
import com.spice.sms139.utilites.Utility;

public class MiddleWare {

	GetFooter footerText = new GetFooter();

	String strMsisdn, strSourceAdderss, strMessage, strUniqueId, strDateTime, strKeyword, strUdh, strDcs, strVmn,
			strImsi, strCircleId;
	String strOutPut = "", strUserName, strPassword, strId;
	String keyword = "Wrong Message";
	String strErrorMessage = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
			+ "currently not available. Please try after sometime.";
	String strCorrectFooterMessage = "For help on SMS Based Ticket Booking, SMS TKT to 139";
	String strErrorFooterMessage = "You can also CALL *139# for PNR Status, Arrival-Departure Time, FARE & SEAT info";

	// String strErrorMessage = "Due to Software Up gradation Service will not
	// be available 22.30 PM on 07-03-2012 to 05.15 AM on 08-03-2012.
	// Inconvenience deeply regretted.";
	public String getOutput(String strId, String strMsisdn, String strSourceAdderss, String strMessage,
			String strUniqueId, String strDateTime, String strKeyword, String strUdh, String strDcs, String strVmn,
			String strImsi, String strCircleId, String strUserName, String strPassword) {
		this.strId = trim(strId);
		this.strMsisdn = trim(strMsisdn);
		this.strSourceAdderss = trim(strSourceAdderss);
		this.strMessage = strMessage;
		this.strUniqueId = strUniqueId;
		this.strDateTime = strDateTime;
		this.strKeyword = strKeyword;
		this.strUdh = strUdh;
		this.strDcs = strDcs;
		this.strVmn = strVmn;
		this.strImsi = strImsi;
		this.strCircleId = strCircleId;
		this.strUserName = strUserName;
		this.strPassword = strPassword;
		getOutput();

		if (strOutPut.equalsIgnoreCase("") || (strOutPut.toLowerCase().indexOf("is currently not available") != -1)
				|| (strOutPut.toLowerCase().indexOf("unable to process your request") != -1)
				|| (strOutPut.toLowerCase().indexOf("not available at the moment") != -1)
				|| (strOutPut.toLowerCase().indexOf("is unavailable at this time") != -1)
				|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
				|| (strOutPut.toLowerCase().indexOf("contact administrator") != -1)
				|| (strOutPut.toLowerCase().indexOf("unknown exception") != -1)
				|| (strOutPut.toLowerCase().indexOf("error") != -1)
				|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
				|| (strOutPut.toLowerCase().indexOf("input data is incorrect") != -1)) {
			// strOutPut = "Dear User, we are not able to process your
			// request currently. Please try after sometime. Sorry for
			// the inconvenience.";
			strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
					+ "currently not available. Please try after sometime."
					+ footerText.getFooterText(strMessage, "sms139");
			
		}
		return strOutPut;
	}

	public void getOutput() {
		System.out.println("in getOutput method " + strMsisdn + " msg=" + strMessage);
		try {
			strId = System.currentTimeMillis() + "" + generateRandom(5);
			DataBaseOperation obj = new DataBaseOperation();
			String strRespMessage = "";
			String strOrgMessage = "";
			strOrgMessage = strMessage;
			strMessage = strMessage.toUpperCase();
			strMessage = strMessage.replaceAll("\\s+", " ");
			strMessage = trim(getMessage(strMessage));
			System.out.println("after msg=" + strMessage);
			obj.insertRequestLog(strDateTime, strId, strUniqueId, strMsisdn, strMessage);
			FormatInputString formatObj = new FormatInputString();
			Logs.getRequestLogs("Before formatting-------> HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
					+ strMsisdn + "$" + strMessage);
			strMessage = formatObj.getFormattedInput(strId, strUniqueId, strMsisdn, strOrgMessage);
			System.out.println("format msg=" + strMessage);
			System.out.println("\n\nFormatted Messages === " + strMessage);

			// Handling MORE keyword for train between two stations.
			if (strMessage.length() == 0) {
				strOutPut = getHelpMessage();
				this.keyword = "Help Message";
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.equalsIgnoreCase("MORE")) {
				System.out.println("inside More keyword=====");
				GetNextTrainInfo getTinfo = new GetNextTrainInfo();
				this.keyword = "TBS Enquiry";
				strOutPut = getTinfo.getInformation(strMsisdn, strMessage);
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			}
			// Handling FOLLOW PNR MESSAGE
			else if (strMessage.startsWith("FOLLOW")) {
				strOutPut = "To get PNR status, please send PNR [10 digit PNR Number] to 139. e.g PNR 1234567890.";
				this.keyword = "FOLLOW PNR";
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
				// Insert Follow Record to Follow Server.
				// obj.inserFollowRecord(strId,strUniqueId,strMsisdn,strMessage);
			}
			// From New Setup
			else if (strMessage.startsWith("ROUTE")) {
				CallBooking routeResp = CallBooking.getCallBookingInstance();
				this.keyword = "Train Route";
				strOutPut = routeResp.hitUrlWith2Params(Utility.getUrlValue("train_route"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("SCHEDULE")) {
				CallBooking schResp = CallBooking.getCallBookingInstance();
				this.keyword = "Train Schedule";
				strOutPut = schResp.hitUrlWith2Params(Utility.getUrlValue("train_schedule"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("TSEAT")) {
				CallBooking tatkalSeatResp = CallBooking.getCallBookingInstance();
				this.keyword = "Tatkal Seat";
				strOutPut = tatkalSeatResp.hitUrlWith2Params(Utility.getUrlValue("tatkal_seat"), strMessage,
						strUniqueId, strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("NEXT")) {
				CallBooking nextTrainResp = CallBooking.getCallBookingInstance();
				this.keyword = "Next Train";
				strOutPut = nextTrainResp.hitUrlWith2Params(Utility.getUrlValue("next_train"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("TRAIN")) {
				if (strOutPut.indexOf(" ") != -1) {
					String[] parts = strMessage.split("\\s");

					List<String> trimmedParts = new ArrayList<String>();
					for (String part : parts) {
						if (part.trim().length() > 0) {
							trimmedParts.add(part);
						}
					}
					if (trimmedParts.size() == 2) {
						this.keyword = "TimeTable Enquiry";
					} else if (trimmedParts.size() == 7) {
						this.keyword = "Train Accommodation";
					} else if (trimmedParts.size() < 6 && isCharacters(trimmedParts.get(0))
							&& isCharacters(trimmedParts.get(1)) && isCharacters(trimmedParts.get(2))) {
						this.keyword = "TBS Enquiry";
					} else if (trimmedParts.size() == 4) {
						this.keyword = "Train Enquiry";
					} else {
						this.keyword = "TBS Enquiry";
					}
				} else {
					this.keyword = "TBS Enquiry";
				}
				CallBooking getStnCodeResp = CallBooking.getCallBookingInstance();
				strOutPut = getStnCodeResp.hitUrlWith2Params(Utility.getUrlValue("stn_code"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
			} else if (strMessage.startsWith("CODE")) {
				CallBooking getStnCodeResp = CallBooking.getCallBookingInstance();
				this.keyword = "CODE Enquiry";
				strOutPut = getStnCodeResp.hitUrlWith2Params(Utility.getUrlValue("stn_code"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("SEAT")) {
				CallBooking getSeatEnq = CallBooking.getCallBookingInstance();
				this.keyword = "Train Accommodation";
				strOutPut = getSeatEnq.hitUrlWith2Params(Utility.getUrlValue("seat_info"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + " " + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("FARE")) {
				CallBooking getFareEnq = CallBooking.getCallBookingInstance();
				this.keyword = "Fair Enquiry";
				strOutPut = getFareEnq.hitUrlWith2Params(Utility.getUrlValue("fare_info"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + " " + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			}
			// Handling Ticket Booking Help Message
			else if (strMessage.equalsIgnoreCase("TKT")) {
				CallBooking bookHelp = CallBooking.getCallBookingInstance();
				this.keyword = "Ticket Help";
				strOutPut = bookHelp.hitUrlWith3Params(Utility.getUrlValue("booking_help"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "." + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			}
			/*
			 * else if (strMessage.startsWith("COACH")){ CallBooking
			 * coachEnqResp= CallBooking.getCallBookingInstance(); this.keyword
			 * = "Coach Enquiry"; strOutPut =
			 * coachEnqResp.getCoachInfo(strMessage,strUniqueId,strMsisdn); //
			 * Insert record to response table obj.insertResponsetLog(strId,
			 * strUniqueId, strMsisdn, strOutPut, keyword); }
			 */
			else if (strMessage.startsWith("REGCODE")) {
				strMessage = strMessage.trim();
				CallBooking bookResp = CallBooking.getCallBookingInstance();
				this.keyword = "User Activation";
				strOutPut = bookResp.hitUrlWith3Params(Utility.getUrlValue("user_activation"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("AD")) {
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "Train Enquiry";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("TN")) {
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "TN Enquiry";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("TIME")) {
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "TimeTable Enquiry";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("ALARM")) {
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "Destination Alarm";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("MEAL") || strMessage.startsWith("FOOD")) {
				System.out.println("Meal Request Message == " + strMessage);
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "Meal Request";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("PLATFORM")) {
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "Platform Enquiry";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("ALERT")) {
				strMessage = strMessage.trim();
				CallBooking adResp = CallBooking.getCallBookingInstance();
				this.keyword = "Destination Alert";
				strOutPut = adResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("SPOT")) {
				strMessage = strMessage.trim();
				CallBooking spotResp = CallBooking.getCallBookingInstance();
				this.keyword = "SPOT Train";
				strOutPut = spotResp.hitUrlWith2Params(Utility.getUrlValue("ad_enquiry"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("REG")) {
				strMessage = strMessage.trim();
				CallBooking bookResp = CallBooking.getCallBookingInstance();
				this.keyword = "User Registration";
				strOutPut = bookResp.hitUrlWith3Params(Utility.getUrlValue("user_registration"), strMessage,
						strUniqueId, strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("TPIN")) {
				strMessage = strMessage.trim();
				CallBooking tpinResp = CallBooking.getCallBookingInstance();
				this.keyword = "TPIN Request";
				strOutPut = tpinResp.hitUrlWith3Params(Utility.getUrlValue("user_tpin"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("BOOKTA") || strMessage.startsWith("BOOKUA")
					|| strMessage.startsWith("BOOKSA") || strMessage.startsWith("BOOKSLA")
					|| strMessage.startsWith("BOOKSPA") || strMessage.startsWith("BOOKBAN")
					|| strMessage.startsWith("BOOKBJT")) {
				strMessage = strMessage.trim();
				CallBooking bookResp = CallBooking.getCallBookingInstance();
				this.keyword = "BOOK Request";
				strOutPut = bookResp.hitUrlWith3Params(Utility.getUrlValue("booking_response"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("PAYTA") || strMessage.startsWith("PAYSA")
					|| strMessage.startsWith("PAYSPA") || strMessage.startsWith("PAYSLA")
					|| strMessage.startsWith("PAYBAN") || strMessage.startsWith("PAYBJT")) {
				strMessage = strMessage.trim();
				CallBooking payResp = CallBooking.getCallBookingInstance();
				this.keyword = "BOOK Ticket";
				strOutPut = payResp.hitUrlWith3Params(Utility.getUrlValue("payment_response"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("BOOK")) {
				strMessage = strMessage.trim();
				CallBooking bookResp = CallBooking.getCallBookingInstance();
				this.keyword = "BOOK Request";
				// strOutPut =
				// bookResp.hitUrlWith3Params(Utility.getUrlValue("booking_response"),
				// strMessage, strUniqueId,
				// strMsisdn);
				// strOutPut = strOutPut + "" +
				// footerText.getFooterText(strMessage, "sms139");
				strOutPut = "Dear User, SMS based Ticket Booking facility has been stopped by Indian Railways. Inconvenience caused is deeply regretted.";
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("PAY")) {
				strMessage = strMessage.trim();
				CallBooking payResp = CallBooking.getCallBookingInstance();
				this.keyword = "BOOK Ticket";
				// strOutPut =
				// payResp.hitUrlWith3Params(Utility.getUrlValue("payment_response"),
				// strMessage, strUniqueId,
				// strMsisdn);
				// strOutPut = strOutPut + "" +
				// footerText.getFooterText(strMessage, "sms139");
				strOutPut = "Dear User, SMS based Ticket Booking facility has been stopped by Indian Railways. Inconvenience caused is deeply regretted.";
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			}

			else if (strMessage.startsWith("CANCEL")) {
				strMessage = strMessage.trim();
				CallBooking BookHelp = CallBooking.getCallBookingInstance();
				strOutPut = BookHelp.hitUrlWith2Params(Utility.getUrlValue("booking_cancel"), strMessage, strUniqueId,
						strMsisdn);
				this.keyword = "CANCEL TKT";
				strOutPut = strOutPut.replaceAll("<br>", "\n");
			}

			else if (strMessage.startsWith("OTP")) {
				strMessage = strMessage.trim();
				CallBooking BookHelp = CallBooking.getCallBookingInstance();
				strOutPut = BookHelp.hitUrlWith2Params(Utility.getUrlValue("otp_booking_cancel"), strMessage,
						strUniqueId, strMsisdn);
				this.keyword = "OTP 4 CANCEL";
				strOutPut = strOutPut.replaceAll("<br>", "\n");
			}

			else if (strMessage.startsWith("CAN")) {
				strMessage = strMessage.trim();
				CallBooking CanReq = CallBooking.getCallBookingInstance();
				this.keyword = "Cancel Request";
				strOutPut = CanReq.hitUrlWith3Params(Utility.getUrlValue("cancel_request"), strMessage, strUniqueId,
						strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("YES")) {
				strMessage = strMessage.trim();
				CallBooking confCan = CallBooking.getCallBookingInstance();
				this.keyword = "Cancel Ticket";
				strOutPut = confCan.hitUrlWith3Params(Utility.getUrlValue("confirm_cancellation"), strMessage,
						strUniqueId, strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// Insert record to response table
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("TRANS")) {
				strMessage = strMessage.trim();
				CallBooking TransStat = CallBooking.getCallBookingInstance();
				this.keyword = "Transaction Status";
				strOutPut = TransStat.hitUrlWith3Params(Utility.getUrlValue("transaction_status"), strMessage,
						strUniqueId, strMsisdn);
				strOutPut = strOutPut + "" + footerText.getFooterText(strMessage, "sms139");
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strRespMessage != null && strRespMessage != "") {
				strOutPut = strRespMessage;
				this.keyword = "Response Message";
				// strOutPut = getComment(strOutPut);
				strOutPut = strOutPut.replaceAll("#", ",");
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMsisdn.equals("919825065037")) {
				strOutPut = "Service is working fine, please go ahead";
				this.keyword = "Pnr Enquiry";
				strOutPut = strOutPut.replaceAll("#", ",");
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("LOUNGE")) {
				strOutPut = "Enjoy world class hospitality in IRCTC Executive Lounge, at Platform No. 16, New Delhi Railway Station. Charges Rs. 350 for 3 Hours. Services: Food & Beverages, Wi-Fi, Recliners, Toilet, TV etc. Extra hour at Rs. 150/hour. Additional services of Wash and Change, Business Center and Convenience Store are also available on additional charges. For Bookings & more details Contact at: +919971992006 or 011-23213487. Book online at www.irctc.co.in or www.railtourismindia.com ";
				this.keyword = "LOUNGE";
				// strOutPut = strOutPut.replaceAll("#", ",");
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			} else if (strMessage.startsWith("IRCTCID")) {
				strOutPut = "Dear User, now you can create IRCTC Login ID from your mobile in 2 steps without Internet & Computer. You can use this ID to book tickets from Mobile. To create Login ID, please SMS REG (First Name) (Last Name) (Date of Birth DDMMYY). Example : REG Sandeep Kumar 110284. Note that the mobile number must not be already registered with IRCTC.";
				this.keyword = "Help Message";
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);

			} else if (strMessage.startsWith("APP") || strMessage.startsWith("AAP") || strMessage.startsWith("ABP")) {
				strOutPut = "Click http://bit.ly/ticketingapp to Download SMS Based Railway Ticket Mobile Booking & Enquiry Application. You can also download the application for Android phones from Play Store or for JAVA/J2ME phones from Get Jar.";
				this.keyword = "APP Download";
				// strOutPut = strOutPut.replaceAll("#", ",");
				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);
			}else if(strMessage.toLowerCase().startsWith("ob")){
				strMessage = strMessage.trim();
				CallBooking TransStat = CallBooking.getCallBookingInstance();
				this.keyword = "Coach Mitra";
				//String pnr=getPnrNUmber(strMsisdn);
				System.out.println("OB msg is "+strMessage);
				strOutPut = TransStat.hitUrlWith2Params(Utility.getUrlValue("coach_mitra"), strMsisdn
						, "",strMessage);
				System.out.println("OB url is hitted::::::::::::"+ strOutPut);
				strOutPut = strOutPut.replaceAll("<br>", "\n");
			}else if(strMessage.toUpperCase().startsWith("MADAD") || strMessage.toUpperCase().startsWith("MADED") || 
					strMessage.toUpperCase().startsWith("MADDAD") || strMessage.toUpperCase().startsWith("MADAT") || strMessage.toUpperCase().startsWith("MADDAT")){
				strMessage = strMessage.trim();
				CallBooking TransStat = CallBooking.getCallBookingInstance();
				this.strKeyword = "MADAD";
				System.out.println("MADAD msg is "+strMessage);
				strOutPut = TransStat.hitUrlWith2Params(Utility.getUrlValue("madad"), strMsisdn
						, "",strMessage);
				System.out.println("MADAD url is hitted::::::::::::"+ strOutPut);
				strOutPut = strOutPut.replaceAll("<br/>", "");
			}else {
				process();
				if ((strOutPut != null && strOutPut.equalsIgnoreCase("Error"))
						|| (strOutPut != null && strOutPut.indexOf("WebException") != -1)
						|| (strOutPut != null && strOutPut.indexOf("Unable to connect to the remote server") != -1)
						|| (strOutPut != null && strOutPut.equalsIgnoreCase("The Data Is Unavailable..."))
						|| (strOutPut != null && strOutPut.indexOf("The operation has timed") != -1)
						|| (strOutPut != null && strOutPut.indexOf("NullReferenceException") != -1)
						|| (strOutPut != null && strOutPut.indexOf("Client found response content type") != -1)
						|| (strOutPut != null && strOutPut.indexOf("System.InvalidOperationException: Client") != -1)) {
					strOutPut = strErrorMessage;
				}

				Logs.getRequestLogs("in else for process()" + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
						+ strMsisdn + "$" + strOutPut);
				strOutPut = strOutPut.replaceAll("#", ",");
				if (strOutPut != null && strOutPut.indexOf("<!DOCTYPE") != -1) {
					strOutPut = strOutPut.substring(0, strOutPut.indexOf("<!DOCTYPE"));
				}
				if (strOutPut.equalsIgnoreCase("") || strOutPut == null) {
					// strOutPut = "Dear User, we are unable to process your
					// request currently. Please try after sometime.";
					strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
							+ "currently not available. Please try after sometime.";
				}
				strOutPut = formatResponse(strOutPut);
				Logs.getRequestLogs("after calling of formatResponse method----> " + strId + "," + strOutPut);
				// Handeling REA Error after dated 28/3/2012
				strOutPut = strOutPut.replaceAll("Thread was being aborted", "");
				strOutPut = strOutPut.replaceAll("Sourde", "Source");
				strOutPut = strOutPut.replaceAll("REA ", "");
				System.out.println("URL Response : === " + strOutPut);
				// Handleing 139 error responses 3/5/2012
				// Not sending ad to Chennai local train Enquiries
				if (strMessage.startsWith("C ")) {
				} else if (strMessage.startsWith("TKT")) {
				}
				// Handling PLATFORM keyword
				else if (strMessage.startsWith("PLATFORM")) {
					// Incorrect STD Code
					if (strOutPut.indexOf("provide correct STD Code") != -1
							|| strOutPut.indexOf("Given station does not appear") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Station STD Code. For Platform Number Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
					}
					// Incorrect Train number
					else if (strOutPut.indexOf("Please enter valid train number") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For Platform Number Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
					} else if (strOutPut.indexOf("invalid syntax") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid syntax For Platform Enquiry, SMS PLATFORM <Train No.> <STD code> to 139. For e.g.: PLATFORM 12012 011. Sorry for the inconvenience.";
					}
					// Multiple errors
					else if (strOutPut.equalsIgnoreCase("") || strOutPut.equalsIgnoreCase("<br>")
							|| strOutPut.indexOf("ERROR") != -1) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					} else {
						strOutPut = strOutPut + "This is subject to change at the time of arrival/departure.<br>"
								+ footerText.getFooterText(strMessage, "sms139");
					}
					this.keyword = "Platform Enquiry";
				}
				// Handling TN keyword
				else if (strMessage.startsWith("TN")) {
					if (strOutPut.toLowerCase().indexOf("wrong train name") != -1
							|| strOutPut.toLowerCase().indexOf("invalid train number") != -1) {
						if (strOutPut.contains("ERROR : Invalid Train No")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train No", "");
						} else if (strOutPut.contains("ERROR : Invalid Train Number")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train Number", "");
						}

						else {
							if (validateTrainNo(strMessage)) {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey.";
							} else {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey.";
							}
						}
					} else {
						if (strOutPut.toLowerCase().indexOf("rea error") != -1) {
							strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
									+ "currently not available. Please try after sometime.";
						} else {

							strOutPut = strOutPut + " " + footerText.getFooterText(strMessage, "sms139");
						}
					}
				}
				// Handling Pnr Enquiry
				else if (keyword.equalsIgnoreCase("Pnr Enquiry")) {
					if (strOutPut.indexOf("Invalid Pnr") != -1 || strOutPut.indexOf("invalid PNR") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid PNR number. For PNR enquiry send PNR [10 digit valid PNR number] to 139. Sorry for the inconvenience. Wishing you safe journey.";
					} else if (strOutPut.indexOf("FetchPNRInformation") != -1 || strOutPut.equals("")) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is currently not available. Please try after sometime.";
					} else {
						System.out.println("Resonse from PNR enquiry === " + strOutPut);
						// strOutPut = getHelpMessage();
						strOutPut = strOutPut + footerText.getFooterText(strMessage, "sms139");
					}
				}
				// Train Enquiry
				else if (keyword.equalsIgnoreCase("Train Enquiry")) {
					// Incorrect STD Code
					if (strOutPut.indexOf("provide correct STD Code") != -1
							|| strOutPut.indexOf("Given station does not appear") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Station STD Code. For Train Arrival Departure enquiry, please CALL 139 or SMS AD <Train No.> <STD code> to 139. For e.g.: AD 12012 011. Sorry for the inconvenience.";
					}
					// Incorrect Train number
					else if (strOutPut.indexOf("Please enter valid train number") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For Train Arrival Departure enquiry, please CALL 139 or SMS AD <Train No> <STD code> to 139. For e.g.: AD 12012 011. Sorry for the inconvenience.";
					}
					// Multiple errors
					else if (strOutPut.equalsIgnoreCase("") || strOutPut.equalsIgnoreCase("<br>")
							|| strOutPut.indexOf("ERROR") != -1) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					}
					// Train Name No
					// OLD code commented by kamal
					/*
					 * if (strMessage.startsWith("TN ")) { if
					 * (strOutPut.indexOf("wrong Train Name") != -1) { if
					 * (validateTrainNo(strMessage)) { strOutPut =
					 * "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey."
					 * ; } else { strOutPut =
					 * "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey."
					 * ; }
					 * 
					 * } else { strOutPut = strOutPut +
					 * footerText.getFooterText(strMessage, "sms139"); } } else
					 * { strOutPut = strOutPut +
					 * footerText.getFooterText(strMessage, "sms139"); }
					 */

					// OLD code commented by kamal

					// if (strMessage.startsWith("TN")) {
					if (strOutPut.toLowerCase().indexOf("wrong train name") != -1
							|| strOutPut.toLowerCase().indexOf("invalid train") != -1) {

						if (strOutPut.contains("ERROR : Invalid Train No")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train No", "");
						} else if (strOutPut.contains("ERROR : Invalid Train Number")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train Number", "");
						}

						else {
							if (validateTrainNo(strMessage)) {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey.";
							} else {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey.";
							}
						}
					} else {
						if (strOutPut.toLowerCase().indexOf("rea error") != -1) {
							strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
									+ "currently not available. Please try after sometime.";
						} /*
							 * else {
							 * 
							 * strOutPut = strOutPut + " " +
							 * footerText.getFooterText(strMessage, "sms139"); }
							 */
					}
					// }
					strOutPut = strOutPut + " " + footerText.getFooterText(strMessage, "sms139");
				}
				// Train Accomodation
				else if (keyword.equalsIgnoreCase("Train Accommodation")) {
					// Incorrect Qouta Code
					if (strOutPut.indexOf("correct Quota Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Quota Code. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience.  Wishing you safe journey.";
					}
					// Icorrect Class code
					else if (strOutPut.indexOf("Please Enter Valid Class Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Class Code. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience.  Wishing you safe journey.";
					}
					// Incorrect station Code
					else if (strOutPut.indexOf("correct STD") != -1 || strOutPut.indexOf("correct Source STD") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Station STD Code. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Date.
					else if (strOutPut.indexOf("enter valid date") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Date. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Train No.
					else if (strOutPut.indexOf("provide Correct Train") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Train Number. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Multiple Errors
					else if (strOutPut.equalsIgnoreCase("your data is not correct<br>")) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid information. For Seat Availability enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}

					if ((strOutPut.toLowerCase().indexOf("rea error") != -1)
							|| (strOutPut.toLowerCase().indexOf("error") != -1)) {
						strOutPut = strErrorMessage;
					}
					strOutPut = strOutPut + footerText.getFooterText(strMessage, "sms139");
				}
				// Fair Enquiry
				else if (keyword.equalsIgnoreCase("Fair Enquiry")) {
					// Incorrect Qouta code
					if (strOutPut.indexOf("correct Quota Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Quota Code. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Icorrect Class code
					else if (strOutPut.indexOf("Please Enter Valid Class Code") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Class Code. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect station Code
					else if (strOutPut.indexOf("correct STD") != -1 || strOutPut.indexOf("correct Source STD") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Station STD Code. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Date.
					else if (strOutPut.indexOf("enter valid date") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Date. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Incorrect Train No.
					else if (strOutPut.indexOf("Please enter Valid Train") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid Train Number. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					}
					// Multiple Errors
					else if (strOutPut.indexOf("Sorry Fare information") != -1
							|| strOutPut.indexOf("your data is not correct") != -1) {
						strOutPut = "Thanks for using 139 Railway Enquiry Service. You have sent an invalid information. For FARE enquiry, please CALL 139 or SMS HELP to 139. Sorry for the inconvenience. Wishing you safe journey.";
					} else {
						strOutPut = strOutPut + footerText.getFooterText(strMessage, "sms139");
					}
				}
				// Time Table Enquiry
				else if (keyword.equalsIgnoreCase("TimeTable Enquiry")) {
					// Invalid Train No.
					if (strOutPut.indexOf("Please Enter valid Train") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN TIME TABLE Enquiry, SMS TIME <Train No.> to 139. For e.g.: TIME 12012. Sorry for the inconvenience. Wishing you safe journey.";
					} else {
						strOutPut = strOutPut + footerText.getFooterText(strMessage, "sms139");
					}
				}
				// SPOT Train
				else if (keyword.equalsIgnoreCase("SPOT Train")) {
					// Incorrect Train number
					if (strOutPut.indexOf("Please Enter valid Train") != -1) {
						strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For Locating the TRAIN, SMS SPOT <Train No.> to 139. For e.g.: SPOT 12012. Sorry for the inconvenience. Wishing you safe journey.";
					} else if ((strOutPut.toLowerCase().indexOf("no record found") != -1)
							|| (strOutPut.toLowerCase().indexOf("unable to process your request") != -1)) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					}
					if (strOutPut.toLowerCase().indexOf("wrong train name") != -1
							|| strOutPut.toLowerCase().indexOf("invalid train") != -1) {

						if (strOutPut.contains("ERROR : Invalid Train No")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train No", "");
						} else if (strOutPut.contains("ERROR : Invalid Train Number")) {
							strOutPut = strOutPut.replaceFirst("ERROR : Invalid Train Number", "");
						}

						else {
							if (validateTrainNo(strMessage)) {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Number. For TRAIN NAME Enquiry, SMS TN [TRAIN NO.] to 139. For e.g.: TN 12012. Sorry for the inconvenience. Wishing you safe journey.";
							} else {
								strOutPut = "Thanks for using 139 Rail Enquiry Service. You have sent an invalid Train Name. For TRAIN Number Enquiry, SMS TN [TRAIN NAME] to 139. For e.g.: TN SHATABDI EXP. Sorry for the inconvenience. Wishing you safe journey.";
							}
						}
					}
					if ((strOutPut.toLowerCase().indexOf("rea error") != -1)
							|| (strOutPut.toLowerCase().indexOf("error") != -1)
							|| (strOutPut.toLowerCase().indexOf("unable to process") != -1)) {
						strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
								+ "currently not available. Please try after sometime.";
					} else {

						strOutPut = strOutPut + " " + footerText.getFooterText(strMessage, "sms139");
					}

				} else if (keyword.equalsIgnoreCase("Wrong Message")) {
					// strOutPut = strOutPut;
				} else {
					strOutPut = strOutPut + footerText.getFooterText(strMessage, "sms139");
				}
				if (strOutPut.indexOf("Thanks for using 139") != -1 && strUniqueId.equals("vod")) {
					strOutPut = strOutPut + strErrorFooterMessage;
				}

				// obj.insertResponsetLog(strId, strUniqueId, strMsisdn,
				// strOutPut, keyword);

			}
			if (strOutPut.equalsIgnoreCase("") || (strOutPut.toLowerCase().indexOf("is currently not available") != -1)
					|| (strOutPut.toLowerCase().indexOf("unable to process your request") != -1)
					|| (strOutPut.toLowerCase().indexOf("not available at the moment") != -1)
					|| (strOutPut.toLowerCase().indexOf("is unavailable at this time") != -1)
					|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
					|| (strOutPut.toLowerCase().indexOf("contact administrator") != -1)
					|| (strOutPut.toLowerCase().indexOf("unknown exception") != -1)
					|| (strOutPut.toLowerCase().indexOf("error") != -1)
					|| (strOutPut.toLowerCase().indexOf("no record found") != -1)
					|| (strOutPut.toLowerCase().indexOf("input data is incorrect") != -1)) {
				// strOutPut = "Dear User, we are not able to process your
				// request currently. Please try after sometime. Sorry for
				// the inconvenience.";
				strOutPut = "Dear User, Thanks for using 139 Rail Enquiry Service. This information is "
						+ "currently not available. Please try after sometime."
						+ footerText.getFooterText(strMessage, "sms139");
			}
			obj.insertResponsetLog(strId, strUniqueId, strMsisdn, strOutPut, keyword);
			Logs.getRequestLogs("Final in getOutput()----> " + "HTTP" + "$" + "HTTP_" + strId + "$" + strUniqueId + "$"
					+ strMsisdn + "$" + strOutPut);
			obj = null;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void process() {
		try {
			MakeURL objMakeURL = new MakeURL();
			Response objResponse = new Response();
			strMessage = strMessage.trim();
			// DataBaseOperation obj=new DataBaseOperation(); // for tracking
			// logs
			if (strMessage.equalsIgnoreCase("help") || strMessage.equalsIgnoreCase("hlp")
					|| strMessage.equalsIgnoreCase("Menu") || strMessage.equalsIgnoreCase("hel")
					|| strMessage.equalsIgnoreCase("option") || strMessage.equalsIgnoreCase("options")
					|| strMessage.equalsIgnoreCase("RAIL") || strMessage.equalsIgnoreCase("Gaddi")
					|| strMessage.equalsIgnoreCase("hel") || strMessage.equalsIgnoreCase("serve")) {
				strOutPut = getHelpMessage();
			} else if (strMessage.equalsIgnoreCase("MHELP")) {
				strOutPut = getMHelpMessage();
			} else {
				strMessage = strMessage.replaceAll("\\n", " ");
				String strErrorResponse = objMakeURL.checkMessage(strMessage);
				if (strErrorResponse.equals("OK")) {
					// obj.updateBBPOurlCall(strId); // Logging BBPO URL call
					// Time.
					Logs.getRequestLogs(
							"Response from MakeUrl--> " + strId + "$" + objMakeURL.getUrl(strMessage, strMsisdn));
					strOutPut = objResponse.makeResponseUrl(objMakeURL.getUrl(strMessage, strMsisdn));
					Logs.getRequestLogs("Response in process--> " + strId + "$" + strOutPut);
					this.keyword = objResponse.getKeyword();
					// obj.updateBBPOresp(strId,strOutPut,keyword); // Logging
					// BBPO Response Time
				} else {
					strOutPut = strErrorResponse;
				}
				// if (strOutPut.indexOf("Sorry invalid PNR.") != -1) {
				// strOutPut += "Pls enter valid 10 digit PNR";
				// }
				if (strOutPut.indexOf(
						"Either PNR Number is not valid or data is not available The Data Is Unavailable") != -1) {
					strOutPut = "Sorry invalid PNR. Please enter valid 10 digit number.";
				} else if (strOutPut.indexOf("REA ERROR :Please provide correct Station Code") != -1) {
					strOutPut = "REA ERROR :Sorry invalid station STD code. Please enter valid station STD code.";
				} else if (strOutPut.indexOf("REA ERROR : Please enter Valid Date") != -1
						&& keyword.equalsIgnoreCase("TimeTable Enquiry")) {
					strOutPut = "REA ERROR :Sorry Train is not running Today. Please Enter valid Train Number.";
				} else if (strOutPut.indexOf("available from REA for FetchPNRInformation") != -1) {
					strOutPut = "Dear User, Information Requested by you is currently not available. Please try after sometime.";
				} else if (strOutPut.indexOf("Data is not available from REA for NTESTrainNTESInformation") != -1) {
					strOutPut = "Dear User, Information Requested by you is currently not available. Please try after sometime.";
				} else if (strOutPut
						.indexOf("Data is not available from REA for FetchTrainBetweenStationsInformations") != -1) {
					strOutPut = "Dear User, Information Requested by you is currently not available. Please try after sometime.";
				}
				Logs.getRequestLogs("In Process method----- " + strId + "," + strOutPut);
				objMakeURL = null;
				objResponse = null;
			}

		} catch (Exception e) {
			Logs.getErrorLogs("In process catch---- " + strId + "," + LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	public synchronized String trim(String strString) {
		if (strString != null && !strString.equals("")) {
			return strString.trim();
		} else {
			return "";
		}
	}

	public boolean validateTrainNo(String strMessage) {
		int intMessageLength = strMessage.length();
		boolean stat = false;
		int intMessageCounter = 0;
		String strMessageCharacter = "", strReturnValue = "";
		while (intMessageCounter < intMessageLength) {
			strMessageCharacter = strMessage.charAt(intMessageCounter) + "";
			if (strMessageCharacter.equals("1") || strMessageCharacter.equals("2") || strMessageCharacter.equals("3")
					|| strMessageCharacter.equals("4") || strMessageCharacter.equals("5")
					|| strMessageCharacter.equals("6") || strMessageCharacter.equals("7")
					|| strMessageCharacter.equals("8") || strMessageCharacter.equals("9")
					|| strMessageCharacter.equals("0")) {
				strReturnValue += strMessageCharacter;
			}
			intMessageCounter++;
		}
		if (strReturnValue.length() > 4) {
			stat = true;
		} else {
			stat = false;
		}

		return stat;
	}

	public String encoding(String encodingString) {

		encodingString = encodingString.replaceAll(" ", "%20");
		encodingString = encodingString.replaceAll(";", "");
		return encodingString;

	}

	public static boolean isCharacters(String s) {
		return s.matches("[a-zA-Z]+");
	}

	public String getMessage(String strMessage) {

		strMessage = getMessagePattern(strMessage,
				"\"|\\(|\\)|<|>|-|=|\\]|\\[|\\?|!|,|&|:|\\+|/|\\*|#|%|\\\\|\\}|\\{|~|'|\\^|\\r|\\n", "");
		strMessage = getMessagePattern(strMessage, "  |Dt|dt", " ");

		return strMessage;

	}

	public String getHelpMessage() {
		String strRetVal = "Railway Enquiry.";
		strRetVal += ",1.PNR Status: PNR (PNR No)";
		strRetVal += ",2.Train Arrival/Departure: AD (Tr No) (Stn STD Code)";
		strRetVal += ",3.Train Current Position: SPOT (Tr No)";
		strRetVal += ",4.Ticket Booking Application: APP";
		strRetVal += ",5.Seat Avail: SEAT (Tr No) (Dt ddmmyy) (From Stn STD Code) (To Stn STD code) (Class) (Quota)";
		strRetVal += ",6.Tatkal Seat Avail: TSEAT (Tr No) (Dt ddmmyy) (To Stn Code) (To Stn Code) (Class)";
		// strRetVal += ",7.Platform Info: PLATFORM (Tr No)(Stn STD Code).";
		this.keyword = "Help Message";
		return strRetVal;
	}

	public String getMHelpMessage() {
		String strRetVal = "Railway Enquiry.";
		strRetVal += ",1.TimeTable: TIME (Tr No)";
		strRetVal += ",2.Train Name/Number: TN (Tr No/ Name)";
		strRetVal += ",3.Next Train: NEXT (From Stn Code) (To Stn Code)";
		strRetVal += ",4.Train Route: ROUTE(Tr No)";
		strRetVal += ",5.Station Code: CODE(Stn Name)";
		strRetVal += ",6.Train Schedule: SCHEDULE(Tr No)";
		strRetVal += ",7.Fare Enquiry: FARE (Tr No) (Dt ddmmyy) (From Stn STD Code) (To Stn STD code) (Class) (Quota)";
		strRetVal += ",8.Trains Between Two Stations: TRAIN (From Stn Code) (To Stn Code)";
		this.keyword = "Help Message";
		return strRetVal;
	}

	public String formatResponse(String retval) {
		MakeURL objM = new MakeURL();
		String numMessage = objM.getMessage(strMessage);
		strMessage = strMessage.toUpperCase();
		if (strMessage.startsWith("T") || strMessage.startsWith("F") || strMessage.startsWith("AD")
				|| strMessage.startsWith("S") || strMessage.startsWith("BOOK") || strMessage.startsWith("PLAN")) {
		} else if (numMessage.length() == 10) {
			retval = "PNR " + numMessage + "," + retval;
		}
		if (retval.indexOf("Name/Number") != -1) {
		} else {
			retval = retval.replaceAll("Number", "");
		}
		if (strMessage.startsWith("SPOT") || strMessage.startsWith("LOCATE")) {
			retval = retval.replaceAll(",", "<br>");
			retval = retval.replaceAll("Schd", "Sch");
			retval = retval.replaceAll("Arrival", "Arr");
			retval = retval.replaceAll("Stopping", "Stop");
			System.out.println("Response is =====" + retval);
			return retval;
		} else if (strMessage.startsWith("PLATFORM")) {
			retval = retval.replaceAll(",", "<br>");
			if (retval.indexOf("Stn To Start") != -1) {
				String strFirst = retval.substring(0, retval.indexOf("Stn To Start"));
				retval = retval.substring(retval.indexOf("Stn To Start")).trim();
				retval = retval.substring(retval.indexOf("Schd Platform")).trim();
				String strFinal = strFirst + retval;
				strFinal = strFinal.replaceAll("<br><br>", "<br>");
				strFinal = strFinal.replaceAll("Stn", "Station");
				strFinal = strFinal.replaceAll("Schd", "Scheduled");
				strFinal = strFinal.replaceAll("Tr :", "Train:");
				return strFinal;
			} else if (retval.indexOf("Schd Arrival") != -1) {
				String strFirst = retval.substring(0, retval.indexOf("Schd Arrival"));
				retval = retval.substring(retval.indexOf("Schd Arrival")).trim();
				retval = retval.substring(retval.indexOf("Schd Platform")).trim();
				String strFinal = strFirst + retval;
				strFinal = strFinal.replaceAll("<br><br>", "<br>");
				strFinal = strFinal.replaceAll("Stn", "Station");
				strFinal = strFinal.replaceAll("Schd", "Scheduled");
				strFinal = strFinal.replaceAll("Tr :", "Train:");
				return strFinal;
			} else {
				return retval;
			}
		} else {
			retval = retval.replaceAll("Date", "Dt");
			retval = retval.replaceAll("CL", "CLASS");
			retval = retval.replaceAll("CHART", "");
			// retval = retval.replaceAll("Train","Tr.");
			retval = retval.replaceAll("TotalFare", "Fare : ");
			retval = retval.replaceAll(",Train Name:", " - ");
			retval = retval.replaceAll(",Tr. Name:", " - ");
			retval = retval.replaceAll("BoardingStation", "BoardingStn");
			retval = retval.replaceAll("ReservationUpTo", ",ReservedUpTo");
			retval = retval.replaceAll("CLASST", "CLT");
			retval = retval.replaceAll("ChartStatus", "ChartStatus");
			retval = retval.replaceAll("P1:Passenger-0,", "P1 ");
			retval = retval.replaceAll("P2:Passenger-1,", "P2 ");
			retval = retval.replaceAll("P3:Passenger-2,", "P3 ");
			retval = retval.replaceAll("P4:Passenger-3,", "P4 ");
			retval = retval.replaceAll("P5:Passenger-4,", "P5 ");
			retval = retval.replaceAll("P6:Passenger-5,", "P6 ");
			retval = retval.replaceAll("P7:Passenger-6,", "P7 ");
			retval = retval.replaceAll("P8:Passenger-7,", "P8 ");
			retval = retval.replaceAll("P9:Passenger-8,", "P9 ");
			retval = retval.replaceAll("P10:Passenger-9,", "P10 ");
			retval = retval.replaceAll(",Booking", " Booking");
			retval = retval.replaceAll(":", " :");
			retval = retval.replaceAll(",,,", ",");
			retval = retval.replaceAll(",,", ",");
			retval = retval.replaceAll("<br>,", ",");
			// retval = retval.replaceAll(",","<br>");
			// retval = retval.replaceAll("Stn","<br>Stn");
			// retval = retval.replaceAll("Schd","<br>Schd");
			// retval = retval.replaceAll("Exp","<br>Exp");
			retval = retval.replaceAll("\\|", ",");
			// System.out.println("\n The time table is=== "+retval);
			retval = retval.replaceAll("DaysOfRuning :Mon,Tue,Wed,Thi,Fri,Sat,Sun :", " Days of Run : ");
			retval = retval.replaceAll("DaysOfRuning :", " Days of Run :");
			if (retval.indexOf("Days Of Running") != -1) {
				String strFinalOutput = "";
				int i = 0;
				while (retval.indexOf("From Stn") != -1) {
					strFinalOutput = strFinalOutput
							+ retval.substring(retval.indexOf("Tr. No"), retval.indexOf("From Stn"));
					// System.out.println(strFinalOutput);
					retval = retval.substring(retval.indexOf("From Stn") + 8).trim();
					// System.out.println(retval);
					i = i + 1;
				}
				System.out.println("RESPONSE ===" + strFinalOutput);
				if (i > 10) // if No. of trains more than 10
				{
					// insert data in DB
					DataBaseOperation object = new DataBaseOperation();
					object.insertTrainBetweenTwoStnInfo(strId, strMsisdn, strMessage, strFinalOutput, i);
					// Truncate 10 trains information
					int j = 0;
					String strFirstMsg = "";
					while (strFinalOutput.indexOf("Tr. No") != -1) {
						if (strFinalOutput.length() > 2) {
							strFirstMsg = strFirstMsg + strFinalOutput.substring(0, strFinalOutput.indexOf(","));
							strFinalOutput = strFinalOutput.substring(strFinalOutput.indexOf(",") + 1).trim();
							strFirstMsg = strFirstMsg + ",";
							// System.out.println(strFirstMsg);
							j++;
							if (j == 10)
								break;
						} else {
							break;
						}
					}
					strFirstMsg = strFirstMsg + "For more trains send MORE to 139.";
					strFirstMsg = strFirstMsg.replaceAll(",", "<br>");
					return strFirstMsg;
				} else {
					strFinalOutput = strFinalOutput.replaceAll(",", "<br>");
					return strFinalOutput;
				}
			} else if (retval.indexOf("ChartStatus") != -1) {
				String strFirst = retval.substring(0, retval.indexOf("BoardingStn"));
				retval = retval.substring(retval.indexOf("BoardingStn")).trim();
				String strSecond = retval.substring(0, retval.indexOf("P1"));
				retval = retval.substring(retval.indexOf("P1")).trim();
				String strThird = retval.substring(0, retval.indexOf("Dt"));
				String strNewThird = "";
				while (strThird.indexOf("Booking") != -1) {
					strNewThird = strNewThird + strThird.substring(0, strThird.indexOf("Booking"));
					strThird = strThird.substring(strThird.indexOf(";,") + 2).trim();
				}
				// System.out.println("strThird is ==== "+strThird);
				strNewThird = strThird + strNewThird;
				strNewThird = strNewThird.replaceAll("P1", ";<br>P1");
				strNewThird = strNewThird.replaceAll("P2", ";<br>P2");
				strNewThird = strNewThird.replaceAll("P3", ";<br>P3");
				strNewThird = strNewThird.replaceAll("P4", ";<br>P4");
				strNewThird = strNewThird.replaceAll("P5", ";<br>P5");
				strNewThird = strNewThird.replaceAll("P6", ";<br>P6");
				strNewThird = strNewThird.replaceAll("CLASS", ";<br>CLASS");
				retval = retval.substring(retval.indexOf("Dt")).trim();
				String strFourth = retval.substring(0, retval.indexOf("ChartStatus"));
				retval = retval.substring(retval.indexOf("ChartStatus")).trim();
				strSecond = strSecond.replaceAll(",;", ",");
				strFirst = strFirst.replaceAll(",", "<br>");
				strFourth = strFourth.replaceAll(",", "<br>");
				strSecond = strSecond.replaceAll(",", "<br>");
				retval = retval.replaceAll(",", "<br>");
				String strFinalmsg = strFirst + strFourth + strSecond + strNewThird + retval;
				System.out.println("strSecond ===" + strSecond);
				strFinalmsg = strFinalmsg.replaceAll("ChartStatus", ";<br>ChartStatus");
				strFinalmsg = strFinalmsg.replaceAll("PREPARED,", "PREPARED");
				// strFinalmsg = strFinalmsg.replaceAll("CLASST,","CLT");
				strFinalmsg = strFinalmsg.replaceAll("<br>;<br>", "<br>");
				strFinalmsg = strFinalmsg.replaceAll(",;<br>", "<br>");
				System.out.println("Response is === " + strFinalmsg);
				return strFinalmsg;
			} else if (retval.indexOf("Days of Run : ") != -1) {
				retval = retval + "(";
				System.out.println("Time Table Message === \n" + retval);
				retval = retval.replaceAll(",Tr. No", "Tr. No");
				String strFirst = retval.substring(0, retval.indexOf("Days of Run"));
				// System.out.println("STRING FIRST==="+strFirst);
				retval = retval.substring(retval.indexOf("Days of Run")).trim();
				// System.out.println("STRING AFTER FIRST === "+retval);
				String strSecond = retval.substring(0, retval.indexOf("("));
				strSecond = strSecond.substring(strSecond.indexOf(":")).trim();
				// System.out.println("STRING SECOND ==="+strSecond);
				retval = retval.substring(retval.indexOf("(")).trim();
				// System.out.println("STRING LAST =="+retval);
				String strNewSecond = "";
				String strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				// System.out.println("after sunday "+strSecond);
				if (strDay.startsWith(": Y")) {
					strNewSecond = "Mon";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				// System.out.println("after Sunday day"+strDay);
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Tue";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Wed";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Thu";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Fri";
				}
				strSecond = strSecond.substring(1);
				strDay = strSecond.substring(0, strSecond.indexOf(","));
				strSecond = strSecond.substring(strSecond.indexOf(",")).trim();
				if (strDay.startsWith("Y")) {
					strNewSecond = strNewSecond + ",Sat";
				}
				if (strSecond.startsWith(",Y")) {
					strNewSecond = strNewSecond + ",Sun";
				}
				strFirst = strFirst.replaceAll(",", "<br>");
				strNewSecond = strNewSecond.replaceAll(",", ";");
				String FinalMessage = strFirst + "Days of Run : " + strNewSecond + strSecond.substring(2);
				FinalMessage = FinalMessage.replaceAll(" Days of Run : ,", "Days of Run : ");
				FinalMessage = FinalMessage.replaceAll(": ;", ": ");
				FinalMessage = FinalMessage.replaceAll("<br>Tr. No", "Train ");
				FinalMessage = FinalMessage.replaceAll("DeptTime", "DeptTime ");
				FinalMessage = FinalMessage.replaceAll("<br>DOJ", " Hrs<br>DOJ");
				FinalMessage = FinalMessage.replaceAll(",Available", "<br>Available");
				FinalMessage = FinalMessage.replaceAll("ArrivalTime", "ArrivalTime ");
				FinalMessage = FinalMessage.replaceAll("Mon;Tue;Wed;Thu;Fri;Sat;Sun", "Daily");
				// FinalMessage = FinalMessage.replaceAll("CLASST","CLT");
				String FirstMessage = FinalMessage.substring(0, FinalMessage.indexOf("DeptTime"));
				FinalMessage = FinalMessage.substring(FinalMessage.indexOf("DeptTime")).trim();
				String SecondMessage = FinalMessage.substring(0, FinalMessage.indexOf("To Stn"));
				SecondMessage = SecondMessage.replaceAll("<br>", " Hrs");
				FinalMessage = FinalMessage.substring(FinalMessage.indexOf("To Stn")).trim();
				FinalMessage = FirstMessage + SecondMessage + FinalMessage;
				FinalMessage = FinalMessage.replaceAll("HrsTo Stn", "<br>To Stn");
				System.out.println("Response is ======= " + FinalMessage);
				return FinalMessage;
			} else if (retval.indexOf("Late by") != -1) {
				retval = retval.replaceAll("Tr  :", "TRAIN :");
				String strFirst = "";
				if (retval.indexOf("Stn To Start") != -1) {
					strFirst = retval.substring(0, retval.indexOf("Stn To Start"));
					retval = retval.substring(retval.indexOf("Stn To Start")).trim();
				} else {
					strFirst = retval.substring(0, retval.indexOf("Schd Arrival"));
					retval = retval.substring(retval.indexOf("Schd Arrival")).trim();
				}
				String strSecond = retval.substring(0, retval.indexOf(",Late by"));
				retval = retval.substring(retval.indexOf(",Late by")).trim();
				strSecond = strSecond.replaceAll(",", ";");
				strSecond = strSecond.replaceAll(";Exp Arrival", " Hrs,Exp Arrival");
				strSecond = strSecond.replaceAll(";Exp Dept", " Hrs,Exp Dept");
				String FinalMsg = strFirst + strSecond + retval;
				FinalMsg = FinalMsg.replaceAll(",Late by", " Hrs,Late by");
				FinalMsg = FinalMsg.replaceAll(",", "<br>");
				System.out.println("Response is =====" + FinalMsg);
				return FinalMsg;
			} else {
				retval = retval.replaceAll("Tr. No :", "TRAIN :");
				retval = retval.replaceAll("Has arrived", "Arrived");
				retval = retval.replaceAll(",", "<br>");
				System.out.println("Response is =====" + retval);
				return retval;
			}
		}
	}

	public long generateRandom(int length) {
		Random random = new Random();
		char[] digits = new char[length];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < length; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return Long.parseLong(new String(digits));
	}

	public String getMessagePattern(String strMessage, String pattern, String replaceWith) {

		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(strMessage);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, replaceWith);
		}
		m.appendTail(sb);
		return sb.toString();

	}
}
