package com.spice.sms139.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spice.sms139.db.DataBaseOperation;
import com.spice.sms139.utilites.LogStackTrace;
import com.spice.sms139.utilites.Logs;

import one39mo.groovy.ParseXml;

public class MOServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String EOL = System.getProperty("line.separator");

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			StringBuilder sb = new StringBuilder();
			Logs.getRequestLogs("In MOServlet");
			BufferedReader in = request.getReader();

			String input = null;
			while ((input = in.readLine()) != null) {
				sb.append(input).append(EOL);
			}
			System.out.println("In MoServlet request is:- " + sb.toString());

			ParseXml parseXml = new ParseXml();

			parseXml.setXmlData(sb.toString());
			parseXml.parse();

			System.out.println("In MoServlet parsed xml is:- " + parseXml.toString());

			String messageId = parseXml.getMessageId();
			String message = parseXml.getMessage();
			String msisdn = parseXml.getMsisdn();
			String shortCode = parseXml.getShortCode();
			String uniqueId = parseXml.getUniqueId();

			String datetime = "NA";
			String keyword = "NA";
			String udh = "NA";
			String dcs = "NA";
			String vmn = "NA";
			String imsi = "NA";
			String username = "idea";
			String password = "cellidea";
			String strUniqueId = "ida";

			System.out.printf("REQUEST RECIEVED AT: %s\n", new Object[] { new Date().toString() });
			System.out.printf("MESSAGE ID: %s\n", new Object[] { messageId });
			System.out.printf("MESSAGE: %s\n", new Object[] { message });
			System.out.printf("MSISDN: %s\n", new Object[] { msisdn });
			System.out.printf("SHORT CODE: %s\n", new Object[] { shortCode });
			System.out.printf("UNIQUE ID: %s\n", new Object[] { uniqueId });

			Logs.getRequestLogs("In MOServlet----> " + msisdn + "$" + shortCode + "$" + message + "$" + strUniqueId
					+ "$" + datetime + "$" + keyword + "$" + udh + "$" + dcs + "$" + vmn + "$" + imsi + "$" + uniqueId
					+ "$" + username + "$" + password);

			DataBaseOperation objDataBaseOperation = new DataBaseOperation();
			objDataBaseOperation.insertRecord(msisdn, shortCode, message, strUniqueId, datetime, keyword, udh, dcs, vmn,
					imsi, uniqueId, username, password);

			response.sendError(200);
		} catch (Exception e) {
			Logs.getFilterErrorLogs(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println(request.getParameter("name"));

	}
}
