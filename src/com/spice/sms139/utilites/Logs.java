package com.spice.sms139.utilites;

import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

public class Logs {

	// static String strLogFilePath = "/home/BMG_logs/";

	private static synchronized void writeLog(String strSubDir, String logString) {

		String strLogFilePath = "";

		if (OsType.isWindows()) {
			strLogFilePath = "E:/sms139_logs/sms139/";
		} else {
			try {
				strLogFilePath = "/home/sms139_logs/sms139/";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String strFileName = "";

		String strDateDir = "";

		Calendar objCalendarRightNow = Calendar.getInstance();

		int intMonth = objCalendarRightNow.get(Calendar.MONTH) + 1;

		int intDate = objCalendarRightNow.get(Calendar.DATE);

		int intHour = objCalendarRightNow.get(Calendar.HOUR_OF_DAY);

		int intMinute = objCalendarRightNow.get(Calendar.MINUTE);

		int intSecond = objCalendarRightNow.get(Calendar.SECOND);

		int intMilliSecond = objCalendarRightNow.get(Calendar.MILLISECOND);

		String strYear = "" + objCalendarRightNow.get(Calendar.YEAR);

		strDateDir = strLogFilePath + "/" + intDate + "-" + intMonth + "-" + strYear;
		createDateDir(strDateDir);
		strFileName = strDateDir + "/" + strSubDir + "_" + intHour + ".log";

		try {

			FileWriter out = new FileWriter(strFileName, true);

			String strLogString = intDate + "-" + intMonth + "-" + strYear + " " + intHour + ":" + intMinute + ":"
					+ intSecond + ":" + intMilliSecond + "#" + logString + "\n";

			out.write(strLogString);

			out.close();

		} catch (Exception ex) {

			// System.out.println("//System.out.println:" + ex.toString());

			ex.printStackTrace();

			System.exit(0);

		}

	}

	private synchronized static void createDateDir(String dateDir) {
		try {
			String folderName = dateDir;
			new File(folderName).mkdirs();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void getErrorLogs(String logString) {
		writeLog("sms139ErrorLog", logString);
	}

	public static void getRequestLogs(String logString) {
		writeLog("sms139RequestLog", logString);
	}

	public static void getDatabaseLogs(String logString) {
		writeLog("sms139DBLog", logString);
	}

	public static void getUserLoginLogs(String logString) {
		writeLog("sms139UserLoginLog", logString);
	}

	public static void getResponseLogs(String logString) {
		writeLog("sms139ResponseLog", logString);
	}

	public static void getFilterLogs(String logString) {
		writeLog("sms139FilterLog", logString);
	}

	public static void getFilterErrorLogs(String logString) {
		writeLog("sms139FilterErrorLog", logString);
	}
}